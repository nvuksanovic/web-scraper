import re
import requests
import pymongo

#Connect with database

uri = "mongodb://127.0.0.1:27017"
client = pymongo.MongoClient(uri)
db = client['rentals']

#Globals
destination_names = []
rental_names = []
sleeps = []
price = []
general_amenities = []


#
# for j in range (0, len(destination_names)):
#     db.destinations.insert({"Destination":"%s"%str(destination_names[j])})


#--------------------------------------------------destinations---------------------------------------------------------

url_start_page = 'https://www.flipkey.com/'

page_destination = requests.get(url_start_page)

#Get destination urls - array

destination_array = []
destination_url = re.findall(r'<li class="destination">[\s]*<a href="/(.*?)/">', page_destination.text, flags = 0)

for i in range(0,len(destination_url)):
    destination_array.append(url_start_page + destination_url[i])

#write array in file urls.txt

text_file = open("urls.txt", "w")
for i in range(0, len(destination_array)):
    text_file.write(str(destination_array[i])+ "\n")
text_file.close()

#Get destination names

destination_with_rentals = re.findall(r'<div class="caption">[\s]*(.*?)[\s]*</div>', page_destination.text, flags = 0)
destination_names = []

for i in range(0, len(destination_with_rentals)):
    destination = re.sub(r'[\s]*<span class="count">.*?</span>', '', str(destination_with_rentals[i]))
    destination_names.append(destination)

#Print destination names

for i in range(0,len(destination_names)):
    print("[", i+1,".","member]")
    print("[Names] : ---->",destination_names[i])

#-------------------------------------------------------rentals---------------------------------------------------------
print("RENTALS")
array_rentals = [line.rstrip('\n') for line in open('urls.txt')]
text_file = open("general-amenities-urls.txt", "w")

for j in range(0, len(array_rentals)):
    url_rentals = array_rentals[j]
    # print(url_rentals)
    page_rentals = requests.get(url_rentals)
    if url_rentals.find("book") == -1:

        #Get all rentals
        rental_names = re.findall(r'<a href="[^\"]+[^>]*/" target="_blank" data-stop-propagation>[\s]*([^<]+)</a>', page_rentals.text, flags=0)

        #Get number of sleeps
        sleeps = re.findall(r'<p>[\s]*.*?<span class="dash"> / </span>[\s]*.*?<span class="dash"> / </span>[\s]*(.*?)</p>', page_rentals.text, flags=0)

        #Get prices
        price = re.findall(r'<p class="price">[\s]*(.*?)[\s]*</p>', page_rentals.text, flags=0)

        #Get urls
        general_amanities = re.findall(r'<a href="https://www.flipkey.com/properties/(.*?)/" target="_blank" data-stop-redirect data-stop-propagation>', page_rentals.text, flags=0)

    else:

        rental_names = re.findall(r'<a href="[^\"]+[^>]*/" target="_blank">(.*?)</a>', page_rentals.text, flags=0)

        sleeps = re.findall(r'<p class="pc-rooms">[\s]*(.*?)[\s]*</p>', page_rentals.text, flags=0)

        price = re.findall(r'<p class="pc-price">[\s]*(.*?)[\s]*</p>', page_rentals.text, flags=0)

    url_array = []

    for i in range(0,len(general_amanities)):
        url_array.append("https://www.flipkey.com/properties/"+general_amanities[i]+"/")

    #Make file for general amenities urls


    for i in range(0, len(url_array)):
        text_file.write(str(url_array[i])+ "\n")


    for i in range(0,len(rental_names)):
        print("[", i+1,".","member]")
        print("[Rental name : ---->]", rental_names[i])
        print("[Sleeps] : ---->",sleeps[i])
        print("[Price] : ---->",price[i])

text_file.close()
print("RENTAL - OVER")
#-------------------------------------------------generalAmenities------------------------------------------------------

print("GENERAL AMENITIES")
array_generalAmenities = [line.rstrip('\n') for line in open('general-amenities-urls.txt')]
# print(array_generalAmenities)

for j in range(0, len(array_generalAmenities)):

    url_rentals = array_generalAmenities[j]
    page_general_amenities = requests.get(url_rentals)

    #Get general amenities

    general_amenities = re.findall(r'<li><i class="icon icon-check.*?"></i>(.*?)</li>', page_general_amenities.text, flags=0)
    general_amenities += re.findall(r'<li>(.*?)</li><li>(.*?)</li>', page_general_amenities.text, flags=0)
    #print(url_rentals)


    for i in range(0,len(general_amenities)):
        print("[", i+1,".","member]")
        print("[General amenities] : ---->",general_amenities[i])
print("GENERAL AMENITIES - OVER")

#----------------------------------------------insertInDB---------------------------------------------------------------


for j in range (0, len(destination_names)):
    db.destinations.insert({"Destination":"%s" % str(destination_names[j]),
          "Rental":"%s" % [str(rental_names[j]),
                           {"Sleeps" : "%s" % str(sleeps[j])},
                            {"Price" : "%s" % str(price[j])},
                             {"General" : "%s" % str(general_amenities[j])}]
         })

print("COLLECTION:")

collection = db['destinations']
dest = collection.find({})
for d in dest:
    print(d)
